class Main {
	constructor(navigation, cart) {
		this.$butResult = document.querySelector('.but-result');
		this.$inputFalse = document.querySelector('.dont-check');
		this.$inputs = document.querySelectorAll('.fild');
		this.$fildResArea = document.querySelector('.area');
		this.$fildResTiles = document.querySelector('.count-tiles');
		this.$fildResPrice = document.querySelector('.price-tile');

		this.$butResult.addEventListener('click', this.validate.bind(this));
	}

	validate() {//validate
		for(let fild of this.$inputs) {
			const valueInput = +fild.value;
			if(isNaN(valueInput)) {
				this.$inputFalse.innerText = 'Данные символы недопустимы. Импользуйте цыфры';
				this.$inputFalse.style.display = 'block';
				return
			} 
			else if (valueInput === 0) {
				this.$inputFalse.innerText = 'Заполните все поля, отмеченные звездочкой';
				this.$inputFalse.style.display = 'block';
				return;
			}
		}
		this.$inputFalse.style.display = 'none';
		this.operation();
	}
	operation(){
		const lengthArea = +document.querySelector('.length-floor').value;
		const widthArea = +document.querySelector('.width-floor').value;
		const lengthTile = (0.01 * +document.querySelector('.length-tile').value);
		const widthTile = (0.01 * +document.querySelector('.width-tile').value);
		const priceTile = +document.querySelector('.tile-price').value;

		const resultArea = lengthArea*widthArea;
		const resultPriceTile = priceTile*resultArea;

		let countTiles = (Math.ceil(lengthArea/lengthTile))*(Math.ceil(widthArea/widthTile));

		this.drawAmountResult(resultArea,countTiles, resultPriceTile); 
	}
	drawAmountResult(area,tiles,tilePrice){
		this.$fildResArea.innerHTML = `Площадь: ${area} m<sup>2</sup>.`;
		this.$fildResTiles.innerHTML = `Количество: ${tiles} плиток.`;
		this.$fildResPrice.innerText = `Стоимость плитки: ${tilePrice}руб.`; 
	}
}
document.addEventListener('DOMContentLoaded',()=> {
	new Main();
});
